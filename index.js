const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const hbs = require("hbs");
require("dotenv").config();
const router = require('./routes/public');
const routerAdmin = require('./routes/admin');

app.use(bodyParser.urlencoded({ extended: true }));
app.use("/", router);
app.use("/admin", routerAdmin);
app.use(express.static(__dirname + "/public"));
app.set('view engine', 'hbs');
app.set("views", __dirname + "/views")
hbs.registerPartials(__dirname + "/views/partials");

const  puerto = process.env.PORT || 3000;
app.listen(puerto, () => {
    console.log(`Servidor corriendo en http://localhost:${puerto}`);
});

