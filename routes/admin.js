const express = require("express");
const router = express.Router();
const { getAll, runQuery, getLastOrder, matriculaExistente, getLastId } = require('../db/conexion');
const fs = require("fs");
const path = require('path');

const multer = require("multer");
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/Images/'); // Guardar las imágenes en la carpeta public/Images
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname); // Usar el nombre original del archivo
    }
});
const upload = multer({ storage: storage });

router.get("/", (req, res) => {
    res.render("admin/index");
});

router.get('/integrantes/listar', async (req, res) => {
    try {
        const integrantes = await getAll('SELECT * FROM integrantes WHERE activo = 1 ORDER BY orden');
        res.render('admin/integrantes/index', {
            integrantes: integrantes
        });
    } catch (error) {
        console.error('Error al obtener los integrantes:', error);
        res.status(500).send('Error al obtener los integrantes');
    }
});

router.get("/integrantes/crear", (req, res) => {
    res.render("admin/integrantes/crearIntegrante");
});

router.post("/integrantes/create", async (req, res) => {
    const lastOrder = await getLastOrder("integrantes");
    const newOrder = lastOrder + 1;
    const { orden, nombre, apellido, matricula, pagina, activo } = req.body;
    const isActive = activo === 'on' ? 1 : 0;

    // Verificar que los campos requeridos no estén vacíos
    if (!nombre || !apellido || !matricula || !pagina) {
        return res.status(400).json({ error: 'Debes completar todos los campos.' });
    }
    if (await matriculaExistente(req.body.matricula)) {
        return res.status(400).json({error: 'La matricula ya existe'});
    }

    try {
        await runQuery(`INSERT INTO integrantes (orden, nombre, apellido, matricula, pagina, activo) VALUES (?, ?, ?, ?, ?, ?)`,
            [newOrder, nombre, apellido, matricula, pagina, isActive]);
        res.json({ success: true });
    } catch (error) {
        console.error('Error al crear el integrante:', error);
        res.status(500).json({ error: 'Error al crear el integrante' });
    }
});

router.get("/tipoMedia/listar", async (req, res) => {
    try {
        // Listar solo los integrantes activos
        const tipoMedia = await getAll('SELECT * FROM tipoMedia WHERE activo = 1 ORDER BY orden');
        res.render('admin/tipoMedia/index', {
            tipoMedia: tipoMedia
        });
    } catch (error) {
        console.error('Error al obtener los tipo media:', error);
        res.status(500).send('Error al obtener tipo media');
    }
});
router.get("/tipoMedia/crear", (req, res) => {
    res.render("admin/tipoMedia/crearTipoMedia");
});

router.post("/tipoMedia/create", async (req, res) => {
    const ultimoOrden = await getLastOrder("tipoMedia");
    const nuevoOrden = ultimoOrden + 1;
    const ultimoId = await getLastId("tipoMedia");
    const nuevoId = ultimoId + 1;

    const { nombre, activo } = req.body;
    const isActive = activo ? 1 : 0;

    if (!nombre) {
        return res.status(400).json({ error: 'Debes completar todos los campos.' });
    }

    try {
        await runQuery(`INSERT INTO tipoMedia (orden, id, nombre, activo) VALUES (?, ?, ?, ?)`,
            [nuevoOrden, nuevoId, nombre, isActive]);
        res.json({ success: true });
    } catch (error) {
        console.error('Error al crear tipo media:', error);
        res.status(500).json({ error: 'Error al crear tipo media' });
    }
});

router.get("/media/listar", async (req, res) => {
    try {
        const media = await getAll(`
            SELECT media.*, tipoMedia.nombre as tipoMediaNombre, integrantes.nombre as integranteNombre, integrantes.apellido as integranteApellido
            FROM media
            LEFT JOIN tipoMedia ON media.tipoMedia = tipoMedia.id
            LEFT JOIN integrantes ON media.matricula = integrantes.matricula
            WHERE media.activo = 1
            ORDER BY media.orden
        `);
        res.render("admin/media/index", {
            media: media
        });
    } catch (error) {
        console.error('Error al obtener los datos de media:', error);
        res.status(500).send('Error al obtener los datos de media');
    }
});

router.get("/media/crear", async (req, res) => {
    try {
        const integrantes = await getAll("SELECT * FROM integrantes WHERE activo = 1 ORDER BY nombre");
        const tipoMedia = await getAll("SELECT * FROM tipoMedia WHERE activo = 1 ORDER BY nombre");
        res.render("admin/media/crearMedia", {
            integrantes: integrantes,
            tipoMedia: tipoMedia,
        });
    } catch (error) {
        console.error('Error al obtener datos para crear media:', error);
        res.status(500).send('Error al obtener datos para crear media');
    }
});

router.post("/media/create", upload.single('imagen'), async (req, res) => {
    try {
        const { url, titulo, tipoMedia, integrante, activo } = req.body;
        const isActive = activo === 'on' ? 1 : 0;
        let newPath = '';
        if (req.file) {
            newPath = `/Images/${req.file.filename}`;
        }
        if (!newPath && !url) {
            return res.status(400).json({ error: 'Debe proporcionar una URL o cargar una imagen.' });
        }
        const ultimoOrden = await getLastOrder("media");
        const nuevoOrden = ultimoOrden + 1;

        const query = `
            INSERT INTO media (src, url, titulo, tipoMedia, matricula, activo, orden)
            SELECT ?, ?, ?, ?, integrantes.matricula, ?, ?
            FROM integrantes
            WHERE integrantes.id = ?`;
        const params = [newPath, url, titulo, tipoMedia, isActive, nuevoOrden, integrante];
        await runQuery(query, params);

        res.json({ success: true });
    } catch (error) {
        console.error('Error al crear media:', error);
        res.status(500).json({ error: 'Error al crear media' });
    }
});


module.exports = router;
